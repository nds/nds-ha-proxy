package nds_ha_proxy

import (
	"encoding/json"
	"fmt"
	"git.ligo.org/nds/nds-ha-proxy/config"
	"log"
	"net/http"
	"net/http/pprof"
)

type ConfigHandlerOutput struct {
	IONodeConfig   map[string]config.IONodeConfig   `json:"io_node_cfg"`
	MetaDataConfig map[string]config.MetaDataConfig `json:"nds_metadata_cfg"`
}

func configHandler(w http.ResponseWriter, _ *http.Request, cfg *config.ConfigAdmin) {
	out := &ConfigHandlerOutput{
		IONodeConfig:   cfg.IONodeConfig,
		MetaDataConfig: cfg.MetaDataConfig,
	}
	data, err := json.Marshal(out)
	if err == nil {
		w.Header().Set("Content-Type", "application/json")
		_, _ = w.Write(data)
	}
}

func statsHandler(w http.ResponseWriter, _ *http.Request, metrics []Metric) {
	w.Header().Set("Content-Type", "text/plain")
	for _, metric := range metrics {
		_, _ = fmt.Fprintf(w, "%s:\t%s\n", metric.Name, metric.Value.String())
	}
}

func AdminMainLoop(cfg *config.ConfigAdmin, metrics *MetricsDatabase) {
	if cfg == nil {
		return
	}

	if cfg.ListenProfile != "" {
		go func(address string) {
			profileMux := http.NewServeMux()
			profileMux.HandleFunc("/debug/pprof/", pprof.Index)
			http.ListenAndServe(address, profileMux)
		}(cfg.ListenProfile)
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		_, _ = w.Write([]byte("Hello world"))
	})
	mux.HandleFunc("/config/", func(w http.ResponseWriter, req *http.Request) {
		configHandler(w, req, cfg)
	})
	mux.HandleFunc("/metrics/", func(w http.ResponseWriter, req *http.Request) {
		statsHandler(w, req, SortMetrics(metrics.CopyStats()))
	})
	mux.HandleFunc("/stats/", func(w http.ResponseWriter, req *http.Request) {
		http.Redirect(w, req, "../metrics/", http.StatusFound)
	})
	server := http.Server{Handler: mux, Addr: cfg.Listen}
	log.Printf("Serving admin interface on %v", cfg.Listen)
	log.Printf("Done serving admin interface, %v", server.ListenAndServe())
}
