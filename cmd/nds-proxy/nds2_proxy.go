package main

import (
	"flag"
	proxy "git.ligo.org/nds/nds-ha-proxy"
	"git.ligo.org/nds/nds-ha-proxy/config"
	"log"
	"os"
)

var configFile = flag.String("config", "nds2_proxy.json", "Path to a config file (json format)")
var sampleConfig = flag.Bool("sample_config", false, "Dump a sample config file to stdout and exit")

func main() {
	flag.Parse()
	if *sampleConfig {
		sample := config.SampleConfig()
		_ = config.SaveConfig(os.Stdout, &sample)
		return
	}
	log.Println("Loading config from ", *configFile)
	cfg, err := config.LoadConfigFile(*configFile)
	if err != nil {
		log.Println("Unable to parse config file ", *configFile)
		log.Fatal(err)
	}

	ndsProxy := proxy.NewProxy(cfg)
	ndsProxy.Run()
}
