package config

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
)

type ConfigAuth struct {
	ServiceName string   `json:"service"`
	FQDN        string   `json:"fqdn"`
	Realm       string   `json:"realm,omitempty"`
	Mechanisms  []string `json:"mechs,omitempty"`
	Krb5Keytab  string   `json:"keytab,omitempty"`
	Krb5Conf    string   `json:"krb5conf,omitempty"`
	Userfile    string   `json:"userfile"`
	AllowList   []string `json:"allowlist,omitempty"`
}

type ConfigServer struct {
	Transport string `json:"transport"`
	Address   string `json:"address"`
	Weight    int    `json:"weight,omitempty"`
}

type ConfigListener struct {
	Auth      *ConfigAuth `json:"auth,omitempty"`
	Listen    string      `json:"listen"`
	Transport string      `json:"transport"`
}

type IONodeConfig struct {
	Network        string            `json:"network"`
	Port           int               `json:"port"`
	MemCache       string            `json:"memcache"`
	FrameLookup    string            `json:"frame_lookup"`
	MetaDataServer string            `json:"metadata_server"`
	Stats          string            `json:"stats"`
	Online         string            `json:"online"`
	RemoteData     string            `json:"remote_data_proxy"`
	Concurrency    string            `json:"concurrency"`
	Epochs         map[string][2]int `json:"epochs,omitempty"`
}

type MetaDataConfig struct {
	Network string `json:"network"`
	Port    int    `json:"port"`
}

type SimpleMetricsConfig struct {
	Listen    string `json:"listen"`
	Transport string `json:"transport"`
}

type ConfigAdmin struct {
	Listen         string                    `json:"listen"`
	Transport      string                    `json:"transport"`
	IONodeConfig   map[string]IONodeConfig   `json:"io_node_cfg"`
	MetaDataConfig map[string]MetaDataConfig `json:"nds_metadata_cfg"`
	ListenProfile  string                    `json:"profile_iface,omitempty"`
}

// Config defines how the proxy should be configured, ports, authentication, ...
type Config struct {
	Servers       []ConfigServer       `json:"servers"`   // servers lists the back end servers that the proxy connects to
	Listeners     []ConfigListener     `json:"listeners"` // Listeners lists the ports to listen for client requests on
	Admin         *ConfigAdmin         `json:"admin,omitempty"`
	SimpleMetrics *SimpleMetricsConfig `json:"simple_metrics,omitempty"`
	SaslPlugins   string               `json:"sasl_plugins,omitempty"`
	OSThreads     int                  `json:"workers,omitempty"`
}

func SampleConfig() Config {
	servers := make([]ConfigServer, 0, 1)
	servers = append(servers, ConfigServer{Transport: "tcp", Address: "localhost:31201", Weight: 1})
	servers = append(servers, ConfigServer{Transport: "tcp", Address: "localhost:31203", Weight: 1})
	listeners := make([]ConfigListener, 0, 2)
	listeners = append(listeners, ConfigListener{
		Auth: &ConfigAuth{
			ServiceName: "nds2",
			FQDN:        "nds2.localhost.localdomain",
			Realm:       "LIGO.ORG",
			Mechanisms:  []string{"GSSAPI"},
			Krb5Keytab:  "./keytab",
			Krb5Conf:    "./krb5.conf",
			Userfile:    "./userfile.txt",
			AllowList:   []string{"127.0.0.1/8"},
		},
		Listen:    ":31200",
		Transport: "tcp",
	})
	listeners = append(listeners, ConfigListener{
		Listen:    ":31202",
		Transport: "tcp",
	})
	admin := &ConfigAdmin{
		Listen:    "localhost:31205",
		Transport: "tcp",
		IONodeConfig: map[string]IONodeConfig{
			"default": {
				Network:        "127.0.0.0",
				Port:           31200,
				MemCache:       "memcached://localhost",
				FrameLookup:    "dcache://localhost:20222",
				MetaDataServer: "nds2://localhost:31201",
				Stats:          "",
				Online:         "",
				RemoteData:     "",
				Concurrency:    "0",
				Epochs: map[string][2]int{
					"ALL":  {0, 1999999999},
					"NONE": {0, 0},
				},
			},
		},
		MetaDataConfig: map[string]MetaDataConfig{
			"default": {
				Network: "127.0.0.0",
				Port:    31201,
			},
		},
		ListenProfile: ":31207",
	}
	MetricsConfig := &SimpleMetricsConfig{
		Listen:    "localhost:31206",
		Transport: "udp",
	}
	cfg := Config{
		Servers:       servers,
		Listeners:     listeners,
		Admin:         admin,
		SimpleMetrics: MetricsConfig,
		SaslPlugins:   "/usr/lib/x86_64-linux-gnu/sasl2/",
		OSThreads:     2,
	}
	return cfg
}

func LoadConfigFile(fname string) (*Config, error) {
	f, err := os.Open(fname)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return LoadConfig(f)
}

func LoadConfig(r io.Reader) (*Config, error) {
	var cfg Config
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func SaveConfig(w io.Writer, c *Config) error {
	data, err := json.Marshal(c)
	if err != nil {
		return err
	}
	var out bytes.Buffer
	err = json.Indent(&out, data, "", "  ")
	if err != nil {
		return err
	}
	_, err = out.WriteTo(w)
	return err
}
