package nds_ha_proxy

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"git.ligo.org/nds/nds-ha-proxy/config"
	"git.ligo.org/nds/nds-ha-proxy/sasl_auth"
	"io"
	"log"
	"net"
	"os"
	"strings"
	"sync"
	"time"
)

type ServerEndpoint interface {
	RunServer(ids chan int64, sl *ServerList, reportListener func(listener net.Listener))
}

type userLineType int

const (
	UserLineAdd     = 0
	UserLineDel     = 1
	UserLineComment = 2
)

type EmptyStruct struct{}

// SaslServerEndpoint represents an endpoint that
// is secured with SASL authentication.  It contains
// an IPAccessList that defines a set of addresses that
// should be accepted as anonymous/unauthenticated addresses
type SaslServerEndpoint struct {
	listen       string
	transport    string
	userListLock sync.Mutex
	userList     *map[string]EmptyStruct
	userListTime time.Time
	allowLists   IPAccessList
}

// UnauthenticatedServerEndpoint represents a endpoint that
// allows all connections and lists the user as Anonymous
type UnauthenticatedServerEndpoint struct {
	listen    string
	transport string
}

// ConnectionAuthorizer represents an authorization source
type ConnectionAuthorizer interface {
	// Authorize is the point where the authorizer can interact with the client
	// to validate the connection.
	// This should return
	//   The username associated with the connection
	//   A blob to be passed to Confirm for this client.  May be nil, it must be safe to
	//     discard as Confirm may or may not be called.
	//   The error state, if error != nil then the username and blob are invalid
	Authorize(io.ReadWriteCloser, int64) (string, interface{}, error)
	// Confirm to the client that the connection is authorized.
	//  This takes the connection associated with the client and the interface blob returned by
	//  Authorize.
	Confirm(io.Writer, interface{}) error
}

type SaslAuthorizer struct {
	server *SaslServerEndpoint
	sasl   sasl_auth.SaslSession
}

type commonRunParams struct {
	Transport      string                             // network type to listen on (tcp, ...)
	Listen         string                             // address to listen on
	Description    string                             // description of the server/proxy
	reportListener func(listener net.Listener)        // callback to report the listener is open
	Ids            chan int64                         // source of connection ids
	ServerList     *ServerList                        // the list of servers to try
	Handler        func(net.Conn, *ServerList, int64) //callback to handle doing the proxy
	// debug/mock parameters leave nil to
	// use the system default functions
	shouldQuit  func() bool
	getListener func(string, string) (net.Listener, error)
	now         func() time.Time
	sleep       func(time.Duration)
}

func (params commonRunParams) ShouldQuit() bool {
	if params.shouldQuit != nil {
		return params.shouldQuit()
	}
	return false
}

func (params commonRunParams) GetListener(network, address string) (net.Listener, error) {
	if params.getListener != nil {
		return params.getListener(network, address)
	}
	return net.Listen(network, address)
}

func (params commonRunParams) Now() time.Time {
	if params.now != nil {
		return params.now()
	}
	return time.Now()
}

func (params commonRunParams) SleepFor(duration time.Duration) {
	if params.sleep != nil {
		params.sleep(duration)
	} else {
		time.Sleep(duration)
	}
}

func commonRunServerLoop(params commonRunParams) {
	l, err := params.GetListener(params.Transport, params.Listen)
	if err != nil {
		log.Printf("Error listening at %s, %s - %v", params.Transport, params.Listen, err)
		return
	}
	defer l.Close()
	log.Print("Connected to ", params.Listen)

	params.reportListener(l)

	for {
		conn, err := l.Accept()
		if err != nil {
			if isErrorTemporary(err) {
				time.Sleep(50 * time.Millisecond)
				continue
			} else {
				log.Printf("listen error (%s) %v", params.Description, err)
				return
			}
		}
		go params.Handler(conn, params.ServerList, <-params.Ids)
	}
}

// The sasl and unauth server had the same code except for types, so
// combine it in a common function.
func commonRunServer(params commonRunParams) {
	backoffList := []time.Duration{
		time.Second * 1,
		time.Second * 5,
		time.Second * 10,
		time.Second * 15,
		time.Second * 30,
		time.Second * 60,
	}
	backoffIndex := -1
	restarts := 0
	for !params.ShouldQuit() {
		start := params.Now()
		commonRunServerLoop(params)
		loopDuration := params.Now().Sub(start)
		backoffIndex++
		restarts++
		if loopDuration < time.Second*5 {
			backoffIndex = 0
		} else if backoffIndex >= len(backoffList) {
			backoffIndex = len(backoffList) - 1
		}
		sleepTime := backoffList[backoffIndex]
		params.SleepFor(sleepTime)
	}
}

func createSaslAuthorizer(server *SaslServerEndpoint) *SaslAuthorizer {
	return &SaslAuthorizer{server: server}
}

func (sa *SaslAuthorizer) Authorize(client io.ReadWriteCloser, connId int64) (string, interface{}, error) {
	netConn, ok := client.(*net.TCPConn)
	if !ok {
		log.Printf("[%d] Cannot authorize a user without a tcp socket\n", connId)
		return "", nil, errors.New("cannot authorize a user without a socket")
	}

	addr := netConn.RemoteAddr().String()
	if sa.server.allowLists.ContainsAddress(addr) {
		return "Anonymous", true, nil
	}

	f, err := netConn.File()
	if err != nil {
		log.Printf("[%d] Unable to retreive socket for SASL auth\n", connId)
		return "", nil, errors.New("unable to retreive socket for SASL auth")
	}
	_, _ = client.Write([]byte("0018"))
	sasl, err := sasl_auth.SaslAuth(f)
	if err != nil {
		log.Printf("[%d] Unable to authorize user\n", connId)
		return "", nil, errors.New("unable to authorize user")
	}

	user := sasl.GetUser()

	if !sa.server.allowedUser(user) {
		log.Printf("[%d] User %s is not allowed, closing connection\n", connId, user)
		_, _ = client.Write([]byte("001B"))
		return "", nil, errors.New("user is not allowed")
	}
	return user, nil, nil
}

func (sa *SaslAuthorizer) Confirm(client io.Writer, authPayload interface{}) error {
	if authPayload == nil {
		return nil
	}
	_, err := client.Write([]byte("0000"))
	return err
}

func (sa *SaslAuthorizer) Close() {
	sa.sasl.Close()
}

type AnonymousAuthorizer struct {
}

func (aa *AnonymousAuthorizer) Authorize(_ io.ReadWriteCloser, _ int64) (string, interface{}, error) {
	return "Anonymous", nil, nil
}

func (aa *AnonymousAuthorizer) Confirm(w io.Writer, _ interface{}) error {
	_, err := w.Write([]byte("0000"))
	return err
}

func createSaslServerEndpoint(cfg config.ConfigListener) (ServerEndpoint, error) {
	ss := &SaslServerEndpoint{listen: cfg.Listen, transport: cfg.Transport}
	userList, userListTime, err := loadUserList(cfg.Auth.Userfile)
	if err != nil {
		log.Fatal("Unable to load initial user auth list")
	}
	ss.userList = userList
	ss.userListTime = userListTime
	ss.allowLists = CreateIPAccessList(cfg.Auth.AllowList)
	go ss.updateUserfile(cfg.Auth.Userfile)
	return ss, nil
}

func createUnauthenticatedServerEndpoint(cfg config.ConfigListener) (ServerEndpoint, error) {
	return &UnauthenticatedServerEndpoint{listen: cfg.Listen, transport: cfg.Transport}, nil
}

func CreateServerEndpoint(cfg config.ConfigListener) (ServerEndpoint, error) {
	if cfg.Auth != nil {
		for _, mech := range cfg.Auth.Mechanisms {
			if mech == "GSSAPI" {
				return createSaslServerEndpoint(cfg)
			}
		}
		return nil, errors.New("unknown auth mechanism")
	}
	return createUnauthenticatedServerEndpoint(cfg)
}

func processUserLine(line string) (string, userLineType) {
	line = strings.Trim(line, " \t\n\r")
	if strings.HasPrefix(line, "#") {
		return "", UserLineComment
	}
	var lineType userLineType
	lineType = UserLineAdd
	if strings.HasPrefix(line, "-") {
		lineType = UserLineDel
		line = strings.Trim(line, "- ")
	}
	return strings.SplitN(line, ":", 2)[0], lineType
}

func loadUserList(fileName string) (*map[string]EmptyStruct, time.Time, error) {
	newList := make(map[string]EmptyStruct, 1500)

	exclude := make([]string, 0, 0)

	fileTime := time.Now()
	fInfo, err := os.Stat(fileName)
	if err != nil {
		return nil, fileTime, err
	}
	fileTime = fInfo.ModTime()
	f, err := os.Open(fileName)
	if err != nil {
		return nil, fileTime, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		user, lineType := processUserLine(line)
		if user == "" {
			continue
		} else if lineType == UserLineAdd {
			newList[user] = EmptyStruct{}
		} else if lineType == UserLineDel {
			exclude = append(exclude, user)
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, fileTime, err
	}

	for _, entry := range exclude {
		if _, ok := newList[entry]; ok {
			delete(newList, entry)
		}
	}
	return &newList, fileTime, err
}

func (ss *SaslServerEndpoint) updateUserfile(fname string) {
	shouldUpdate := func(t time.Time) bool {
		ss.userListLock.Lock()
		defer ss.userListLock.Unlock()

		return t.Unix() > ss.userListTime.Unix()
	}

	swapLists := func(newList *map[string]EmptyStruct, t time.Time) {
		ss.userListLock.Lock()
		defer ss.userListLock.Unlock()

		ss.userList = newList
		ss.userListTime = t
	}

	for {
		time.Sleep(1 * time.Minute)
		fInfo, err := os.Stat(fname)
		if err != nil {
			continue
		}
		if !shouldUpdate(fInfo.ModTime()) {
			continue
		}
		newList, timestamp, err := loadUserList(fname)
		if err != nil {
			continue
		}
		swapLists(newList, timestamp)
		log.Println("Installed a new user list")
	}
}

func (ss *SaslServerEndpoint) allowedUser(username string) bool {
	username = strings.SplitN(username, "@", 2)[0]

	ss.userListLock.Lock()
	defer ss.userListLock.Unlock()

	if _, ok := (*ss.userList)[username]; ok {
		return true
	}
	if _, ok := (*ss.userList)["valid-user"]; ok {
		return true
	}
	if _, ok := (*ss.userList)["anonymous"]; ok {
		return true
	}
	return false
}

func (ss *SaslServerEndpoint) runProxy(client io.ReadWriteCloser, sl *ServerList, clientLabel string, connId int64) {
	defer client.Close()
	auth := createSaslAuthorizer(ss)
	defer auth.Close()
	RunNDS2Proxy(client, sl, clientLabel, connId, auth)
}

func (ss *SaslServerEndpoint) RunServer(ids chan int64, sl *ServerList, reportListener func(listener net.Listener)) {
	commonRunServer(commonRunParams{
		Transport:      ss.transport,
		Listen:         ss.listen,
		Description:    "sasl server",
		reportListener: reportListener,
		Ids:            ids,
		ServerList:     sl,
		Handler: func(conn net.Conn, list *ServerList, i int64) {
			ss.runProxy(conn, list, conn.RemoteAddr().String(), i)
		},
	})
}

func (uas *UnauthenticatedServerEndpoint) runProxy(client io.ReadWriteCloser, sl *ServerList, clientLabel string, conn_id int64) {
	defer client.Close()
	RunNDS2Proxy(client, sl, clientLabel, conn_id, &AnonymousAuthorizer{})
}

func (uas *UnauthenticatedServerEndpoint) RunServer(ids chan int64, sl *ServerList, reportListener func(listener net.Listener)) {
	commonRunServer(commonRunParams{
		Transport:      uas.transport,
		Listen:         uas.listen,
		Description:    "unauth server",
		reportListener: reportListener,
		Ids:            ids,
		ServerList:     sl,
		Handler: func(conn net.Conn, list *ServerList, i int64) {
			uas.runProxy(conn, list, conn.RemoteAddr().String(), i)
		},
	})
}

func doProxy(dst io.Writer, src io.Reader, done func()) {
	defer done()
	_, _ = io.Copy(dst, src)
}

func RunNDS2Proxy(client io.ReadWriteCloser, sl *ServerList, clientLabel string, connId int64, auth ConnectionAuthorizer) {
	const AUTHORIZE = "authorize\n"
	var DaqdOk = []byte("0000")

	buf := make([]byte, len(AUTHORIZE))
	end := len(buf)
	count := 0
	for count < end {
		n, err := client.Read(buf[count:end])
		if err != nil {
			return
		}
		count += n
	}
	if !bytes.Equal([]byte(AUTHORIZE), buf) {
		return
	}
	user, payload, err := auth.Authorize(client, connId)
	if err != nil {
		log.Printf("[%d] %s\n", connId, err)
		return
	}
	var server net.Conn
	for {
		serverInfo, err := sl.NextServer()
		if err != nil {
			log.Print(err)
			return
		}
		server, err = net.Dial(serverInfo.transport, serverInfo.address)
		if err != nil {
			log.Printf("[%d] Unable to connect to %s", connId, serverInfo.address)
			sl.MarkServerDown(serverInfo)
			continue
		}

		var tmp [4]byte
		_, _ = server.Write([]byte(AUTHORIZE))
		_, _ = server.Read(tmp[:])
		if bytes.Compare(tmp[:], DaqdOk) != 0 {
			// We got an error message back from the node
			// fail the node for now.
			log.Printf("Server %s returned bad result on authenticate request [%s], dropping temporarily", serverInfo.address, string(tmp[:]))
			sl.MarkServerDown(serverInfo)
			// can we do this in a better way so that we don't forget this?
			server.Close()
		}

		log.Printf("[%d] Connecting client <%s> %s to %s\n", connId, user, clientLabel, serverInfo.address)
		break
	}
	defer server.Close()
	if err = auth.Confirm(client, payload); err != nil {
		log.Printf("[%d] Client %s error confirming request, closing down\n", connId, clientLabel)
	}

	var respBuffer [4]byte
	_, _ = server.Write([]byte(fmt.Sprintf("__remote_user {%s};\n", user)))
	_, _ = io.ReadFull(server, respBuffer[:])

	var wg sync.WaitGroup
	wg.Add(2)
	// make sure that closing one side of the conversation closes the other
	doneCb := func() {
		_ = server.Close()
		_ = client.Close()
		wg.Done()
	}
	go doProxy(client, server, doneCb)
	go doProxy(server, client, doneCb)
	wg.Wait()
	log.Printf("[%d] Client %s connection closed down\n", connId, clientLabel)
}
