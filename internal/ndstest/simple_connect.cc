#include "simple_connect.h"

#include <nds.hh>

extern "C"
{
    int simple_connect_and_quit(const char* hostname, int port)
    try
    {
        if (!hostname)
        {
            return 0;
        }
        NDS::connection conn(hostname, port);
        return 1;
    }
    catch(...)
    {
        return 0;
    }

    int simple_multi_connect_and_quit(const char* hostname, int port)
    try
    {
        if (!hostname)
        {
            return 0;
        }
        NDS::connection conn(hostname, port);

        NDS::connection conn2(hostname, port);
        return 1;
    }
    catch(...)
    {
        return 0;
    }
}