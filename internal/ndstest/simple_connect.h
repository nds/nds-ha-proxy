#ifndef TESTING_SIMPLE_CONNECT_H
#define TESTING_SIMPLE_CONNECT_H

#ifdef __cplusplus
extern "C" {
#endif

extern int simple_connect_and_quit(const char* hostname, int port);

extern int simple_multi_connect_and_quit(const char* hostname, int port);

#ifdef __cplusplus
}
#endif

#endif  /* TESTING_SIMPLE_CONNECT_H */