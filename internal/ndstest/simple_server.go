package ndstest

import (
	"bufio"
	"context"
	"log"
	"net"
)

type SimpleNds2Server struct {
	ctx context.Context
	l   net.Listener

	clientCb func(string)
}

func NewSimpleNds2ServerWithCB(ctx context.Context, cb func(string)) (*SimpleNds2Server, error) {
	l, err := net.Listen("tcp", "localhost:0")
	if err != nil {
		return nil, err
	}
	server := &SimpleNds2Server{ctx: ctx, l: l, clientCb: cb}
	go server.mainLoop()
	return server, nil
}

func NewSimpleNds2Server(ctx context.Context) (*SimpleNds2Server, error) {
	dummyCb := func(string) {
	}
	return NewSimpleNds2ServerWithCB(ctx, dummyCb)
}

func (s *SimpleNds2Server) isDone() bool {
	select {
	case _ = <-s.ctx.Done():
		return true
	default:
		return false
	}
}

func (s *SimpleNds2Server) waitForDone() {
	_ = <-s.ctx.Done()
}

func (s *SimpleNds2Server) mainLoop() {

	go func() {
		s.waitForDone()
		_ = s.l.Close()
	}()

	for !s.isDone() {
		conn, err := s.l.Accept()
		if err != nil {
			break
		}
		go s.clientLoop(conn)
	}
}

func (s *SimpleNds2Server) clientLoop(conn net.Conn) {
	defer func() {
		_ = conn.Close()
		log.Printf("SimpleNds2Server::clientLoop closed client socket")
	}()
	scanner := bufio.NewScanner(conn)

	for scanner.Scan() {
		done := false
		input := scanner.Text()
		log.Printf("Received command '%s'", input)
		switch input {
		case "authorize":
			_, _ = conn.Write([]byte("0000"))
		case "server-protocol-version;":
			_, _ = conn.Write([]byte("0000"))
			_ = writeUInt32(conn, 1)
		case "server-protocol-revision 6;":
			_, _ = conn.Write([]byte("0000"))
			_ = writeUInt32(conn, 6)
		case "quit;":
			done = true
		default:
			log.Printf("Recevied '%s'", input)
			_, _ = conn.Write([]byte("0017"))
		}
		s.clientCb(input)
		if done {
			break
		}
	}
}

func (s *SimpleNds2Server) ListenDetails() (string, string) {
	return s.l.Addr().Network(), s.l.Addr().String()
}

func (s *SimpleNds2Server) Address() string {
	return s.l.Addr().String()
}
