package ndstest

import (
	"encoding/binary"
	"io"
)

func writeUInt32(w io.Writer, val uint32) error {
	var buffer [4]byte

	binary.BigEndian.PutUint32(buffer[:], val)
	n, err := w.Write(buffer[:])
	if n != 4 {
		return err
	}
	return nil
}

func readUInt32(r io.Reader) (uint32, error) {
	var buffer [4]byte

	n, err := io.ReadFull(r, buffer[:])
	if n != 4 {
		return 0, err
	}
	return binary.BigEndian.Uint32(buffer[:]), nil
}
