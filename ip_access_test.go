package nds_ha_proxy

import (
	"strings"
	"testing"
)

func TestCreateIPAccessList(t *testing.T) {
	type args struct {
		inputs []string
	}
	tests := []struct {
		name     string
		args     args
		expCount int
		contains []string
		excludes []string
	}{
		{
			name: "empty list",
			args: args{
				inputs: nil,
			},
			expCount: 0,
			contains: []string{},
			excludes: []string{"127.0.0.1"},
		},
		{
			name: "single ip list, not network",
			args: args{
				inputs: []string{"127.0.0.1"},
			},
			expCount: 1,
			contains: []string{"127.0.0.1"},
			excludes: []string{"127.0.0.2"},
		},
		{
			name: "single ip",
			args: args{
				inputs: []string{"127.0.0.1/32"},
			},
			expCount: 1,
			contains: []string{"127.0.0.1"},
			excludes: []string{"127.0.0.2"},
		},
		{
			name: "multiple networks",
			args: args{
				inputs: []string{"127.0.0.1/8", "192.168.0.1/24"},
			},
			expCount: 2,
			contains: []string{"127.0.0.1", "127.1.0.1", "192.168.0.250"},
			excludes: []string{"128.0.0.2", "192.167.1.1"},
		},
		{
			name: "single ip6",
			args: args{
				inputs: []string{"::1"},
			},
			expCount: 1,
			contains: []string{"::1"},
			excludes: []string{"::2"},
		},
		{
			name: "mixed ip 4 & 6",
			args: args{
				inputs: []string{"::1/128", "192.168.0.1/24"},
			},
			expCount: 2,
			contains: []string{"::1", "192.168.0.5"},
			excludes: []string{"::2", "127.0.0.1"},
		},
	}

	for _, testCase := range tests {
		accessList := CreateIPAccessList(testCase.args.inputs)
		if len(accessList.entries) != testCase.expCount {
			t.Fatal("Mismatch on networks")
		}
		if len(testCase.contains) > 0 {
			for _, ipAsString := range testCase.contains {
				if !accessList.ContainsAddress(ipAsString) {
					t.Fatalf("Networks specified by %v did not include %s when using ContainsAddress", testCase.args.inputs, ipAsString)
				}
				addressWithPort := ipAsString + ":42"
				if strings.HasPrefix(ipAsString, ":") {
					addressWithPort = "[" + ipAsString + "]:42"
				}
				if !accessList.ContainsAddress(addressWithPort) {
					t.Fatalf("Networks specified by %v did not include %s when using ContainsAddress", testCase.args.inputs, addressWithPort)
				}
			}
		}
		if len(testCase.excludes) > 0 {
			for _, ipAsString := range testCase.excludes {
				if accessList.ContainsAddress(ipAsString) {
					t.Fatalf("Networks specified by %v did not exclude %s", testCase.args.inputs, ipAsString)
				}
			}
		}
	}
}
