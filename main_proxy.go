package nds_ha_proxy

import (
	"git.ligo.org/nds/nds-ha-proxy/config"
	"git.ligo.org/nds/nds-ha-proxy/sasl_auth"
	"log"
	"net"
	"sync"
)

type Proxy struct {
	lock      sync.Mutex
	cfg       *config.Config
	listeners []net.Listener
}

func listenerListFullyPopulated(listeners []net.Listener) bool {
	for _, val := range listeners {
		if val == nil {
			return false
		}
	}
	return true
}

func NewProxy(cfg *config.Config) *Proxy {
	return &Proxy{cfg: cfg, listeners: make([]net.Listener, 0, 0)}
}

func (p *Proxy) Stop() {
	p.lock.Lock()
	defer p.lock.Unlock()
	for i, _ := range p.listeners {
		p.listeners[i].Close()
	}
}

func (p *Proxy) GetListener(index int) net.Listener {
	p.lock.Lock()
	defer p.lock.Unlock()
	return p.listeners[index]
}

func (p *Proxy) Running() bool {
	p.lock.Lock()
	defer p.lock.Unlock()
	return len(p.listeners) > 0
}

func (p *Proxy) Run() {
	// Sasl support may be optional
	if sasl_auth.InitSasl(p.cfg) {
		defer sasl_auth.CloseSasl()
	}

	serverList := CreateServerList(p.cfg)

	metrics := NewMetricsDatabase()
	metricsServer := StatsServerFactory(p.cfg.SimpleMetrics, metrics)

	go serverList.ServerCheckLoop(metrics)

	idCounter := int64(0)
	idChan := make(chan int64)
	go func() {
		for {
			idChan <- idCounter
			idCounter++
		}
	}()

	listenerWg := &sync.WaitGroup{}

	listeners := make([]net.Listener, len(p.cfg.Listeners))
	recordNewListener := func(index int) func(net.Listener) {
		return func(listener net.Listener) {
			p.lock.Lock()
			defer p.lock.Unlock()

			listeners[index] = listener
			if listenerListFullyPopulated(listeners) {
				p.listeners = listeners
			}
		}
	}

	for endpointIndex, listenCfg := range p.cfg.Listeners {
		endpoint, err := CreateServerEndpoint(listenCfg)
		if err != nil {
			log.Fatal(err)
		}
		listenerWg.Add(1)
		go func(curEndpoint ServerEndpoint, index int) {
			defer listenerWg.Done()
			curEndpoint.RunServer(idChan, serverList, recordNewListener(index))
		}(endpoint, endpointIndex)
	}

	go AdminMainLoop(p.cfg.Admin, metrics)

	go metricsServer.ListenAndServe()

	listenerWg.Wait()
	log.Println("All endpoints have closed, exiting")
}
