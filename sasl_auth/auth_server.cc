/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "auth_server.hh"
#include "buffer.hh"
// use a std::vector instead
//#include "lcl_array.hh"
#include "socket_api.hh"
//#include "sysexcept.hh"
#include <cstdio>
#include <cstring>
#include <iostream>
#include <vector>
#include <sasl/saslutil.h>

#include <stdexcept>

#define USER_VALID "valid-user"
#define USER_ANON "anonymous"

using namespace sends;
using namespace std;

namespace {

std::vector<char> _sasl_path;

}

//======================================  Sasl callbacks.
extern "C" {

static int sasl_my_log(void *context __attribute__((unused)), int prio,
                       const char *msg) {
    if (!msg)
        return SASL_BADPARAM;
    printf("SASL: %s\n", msg);
    return SASL_OK;
}

static int getpath(void *context __attribute__((unused)), const char **path) {
    if (!path)
        return SASL_BADPARAM;
    if (_sasl_path.size() > 1) {
        *path = &_sasl_path[0];
    } else {
#ifdef __sun__
        *path = "/usr/lib/sasl";
#else
#ifdef __x86_64
        *path = "/usr/lib64/sasl2";
#else
        *path = "/usr/lib/sasl2";
#endif
#endif
    }
    return SASL_OK;
}

static int getrealm(void *context __attribute__((unused)), const char **realm) {
    if (!realm)
        return SASL_BADPARAM;
    *realm = auth_server::refGlobal().realm();
    return SASL_OK;
}

static sasl_callback_t callbacks[] = {
    {SASL_CB_LOG, (int (*)()) & sasl_my_log, NULL},
    {SASL_CB_GETREALM, (int (*)()) & getrealm, NULL},
    {SASL_CB_GETPATH, (int (*)()) & getpath, NULL},
    {SASL_CB_LIST_END, NULL, NULL}};
}

//======================================  global sasl system devic
sasl_server auth_server::global;

//======================================  global sasl constructor
sasl_server::sasl_server(void) : mInitialized(false) {}

//======================================  Go away at end
sasl_server::~sasl_server(void) {}

//======================================  Add a protocol
void sasl_server::addProtocol(const std::string &p) {
    if (mMechanism.empty()) {
        mMechanism = p;
    } else {
        mMechanism += " ";
        mMechanism += p;
    }
}

//======================================  Set the fully qualified domain name
void sasl_server::setFQDN(const std::string &d) { mFQDN = d; }

//======================================  Set the Realm
void sasl_server::setRealm(const std::string &r) { mRealm = r; }

//======================================  Set the service name
void sasl_server::setServer(const std::string &p) { mServer = p; }

//======================================  Initialize sasl
void sasl_server::initialize(const char *server, const char *plugin_path) {
    if (!server) {
        throw std::runtime_error("Invalid service value for the SASL server");
    }
    _sasl_path.clear();
    if (plugin_path) {
        for (int cur = 0; plugin_path[cur]; ++cur) {
            _sasl_path.push_back(plugin_path[cur]);
        }
    }
    _sasl_path.push_back('\0');
    int result =
        sasl_server_init(callbacks,        /* Callbacks supported */
                         server); /* Name of the application */

    if (result != SASL_OK) {
        throw std::runtime_error("Unable to initialize SASL server");
    }
}
void sasl_server::clean_up() {
    sasl_done();
}

//======================================  Constructor
auth_server::auth_server(auth_type at)
    : mAuthType(at), mContext(0), mDebug(1000) {}

//======================================  Destructor
auth_server::~auth_server(void) { close(); }

//======================================  Authenticate client on socket s
void auth_server::authenticate(socket_api &s, buffer &b) {
    //----------------------------------  Blow away existing data, j.i.c.
    mAuthUser.erase();

    //----------------------------------  Make sure SASL has been initialized
    if (mAuthType == kSasl) {
        if (mDebug > 0) cout << "selecting SASL auth" << endl;
        auth_sasl(s, b);
    } else {
        throw runtime_error("No anonymous user in non-authenticated server");
    }
}

//======================================  Authenticate client using sasl
void auth_server::auth_sasl(socket_api &s, buffer &b) {
    if (mDebug > 1) {
        cout << "entering auth_sasl mContext = " << mContext << endl;
    }
    //----------------------------------  Make a context for this connection.
    open();


    int result = SASL_OK;
    try {
        const char *out = NULL;
        unsigned outlen = 0;

        std::string mech = getMechanismList();

        if (mDebug > 1) cout << "mech list is " << mech << endl;

        // cerr << "auth_server: Mechanisms are " << out << endl;
        put(s, mech.c_str(), mech.size());
        if (mDebug > 1) cout << "sent mech list" << endl;
        get(s, b);
        if (mDebug > 1) cout << "received selection from user" << endl;

        string select = b.data(); // get the selected mechanism.
        size_t extra = b.size() - select.size();
        //
        // note about a bit of weirdness here:
        // the old client sent the mechanism name only (no null, no first
        // string).
        // when compiled with-gssapi it sent mechanism name with null
        // termination.
        // but no valid first string. the step1=false causes the start output to
        // be
        // skipped to compensate for this.
        char *xdata = NULL;
        bool step1 = false;
        if (extra > 1) {
            extra--;
            xdata = b.data() + select.size() + 1;
            step1 = true;
        } else {
            extra = 0;
        }
        if (mDebug > 1)  cout << "starting sasl server" << endl;
        result = sasl_server_start(mContext, /* context */
                                   select.c_str(),
                                   xdata, /* optional string from the client*/
                                   extra, /* and it's length */
                                   &out,  /* The output of the library.
                                             Might not be NULL terminated */
                                   &outlen);
        if (mDebug > 1)
            cout << "auth_server: Start negotiation. Result: " << result
                 << " null-out: " << ((out == NULL) ? "yes" : "no")
                 << " outlen: " << outlen << endl;

        if (mDebug > 1) cout << "starting sasl loop" << endl;
        //------------------------------  Loop over steps.
        while (result == SASL_CONTINUE) {
            if (out && step1) {
                put(s, out, outlen);
            } else if (mDebug > 1) {
                cout << "auth_server: undefined response suppressed" << endl;
            }
            step1 = true;
            b.clear();
            get(s, b);
            out = NULL;
            result =
                sasl_server_step(mContext, b.data(), /* what the client gave */
                                 b.size(),           /* it's length */
                                 &out,               /* allocated by library*/
                                 &outlen);

            if (mDebug > 1)
                cout << "auth_server: Step interaction. Result: " << result
                     << " null out: " << (out == 0) << endl;
        }


        //------------------------------  Check for successful authentication.
        if (result != SASL_OK) {
            cerr << sasl_errdetail(mContext) << endl;
            throw runtime_error("auth_server: authentication failure");
        } else if (mDebug > 1) {
            cout << "auth_server: negotiation complete" << endl;
        }

        //------------------------------  Get the user name, data.
        if (mDebug > 1)
            cout << "auth_server: Get user name" << endl;
        const void *userpar = 0;
        if (sasl_getprop(mContext, SASL_USERNAME, &userpar) != SASL_OK ||
            !userpar) {
            throw runtime_error("No username specified");
        }
        string user(reinterpret_cast<const char *>(userpar));
        if (mDebug > 1) cout << "auth_server: authenticated user " << user << endl;
        mAuthUser = user;
        //if (User_List.known(user)) {
        //    mAuthUser = User_List[user];
        //} else if (User_List.known(USER_VALID)) {
        //    cout << "auth_server: User " << user << " aliased to " << USER_VALID
        //         << "." << endl;
        //    mAuthUser = User_List[USER_VALID];
        //} else {
        //    throw runtime_error(string("Unknown user: ") + user);
        //}

    } catch (exception &e) {
        close();
        throw;
    }
}

//======================================  Authenticate client on socket s
void auth_server::close(void) {
    if (mContext)
        sasl_dispose(&mContext);
    mAuthUser.erase();
}

//======================================  Add a protocol
std::string auth_server::getMechanismList(void) const {
    const char *out;
    unsigned outlen = 0;
    int nMech = 0;

    int result = sasl_listmech(mContext, /* The context for this connection */
                               NULL,     /* not supported */
                               NULL,     /* What to prepend the string with */
                               " ",      /* What to separate mechanisms with */
                               NULL,     /* What to append to the string */
                               &out,     /* The produced string. */
                               &outlen,  /* length of the string */
                               &nMech    /* # of mechanisms in string */
                               );
    string mech(out, outlen);
    if (mDebug > 1) {
        cout << "auth_server: Get mechanism list. Result = " << result << endl;
        if (outlen)
            cout << "auth_server: Available mechanisms: " << mech << endl;
    }

    if (result != SASL_OK || !nMech) {
        cerr << "SASL details: " << sasl_errdetail(mContext) << endl;
        throw runtime_error("No valid authentication mechanisms");
    }

    string mechList = refGlobal().getMechanism();
    if (!mechList.empty()) {
        string::size_type inx = 0;
        for (int i = 0; i < nMech; i++) {
            string::size_type ispc = mech.find(' ', inx);
            if (ispc == string::npos)
                ispc = mech.size();
            std::string imech = mech.substr(inx, ispc - inx);
            while (ispc < mech.size() && mech[ispc] == ' ')
                ispc++;

            //--------------------------  Remove mechanism if not allowed
            if (mechList.find(imech) == string::npos) {
                mech.erase(inx, ispc - inx);
            }

            //--------------------------  Bump pointer past allowed mechanism
            else {
                inx = ispc;
            }
        }
        if (mDebug)
            cout << "auth_server: Filtered mechanism list: " << mech << endl;
    }
    return mech;
}

//======================================  Authenticate client on socket s
void auth_server::open(void) {
    int result =
        sasl_server_new(global.service(), /* Registered service name */
                        global.fqdn(),    /* fully qualified domain name;
                                             NULL says use gethostname() */
                        NULL,             /* The user realm used for password
                                             lookups; NULL = default to serverFQDN
                                             Note: This does not affect Kerberos */
                        NULL, NULL,       /* IP Address information strings */
                        NULL,             /* Callbacks for this connection */
                        0, /* security flags (security layers are enabled
                            * using security properties, separately) */
                        &mContext);

    if (mDebug > 1)
        cout << "auth_server: Tried to open SASL context. Result = " << result
             << endl;

    if (result != SASL_OK) {
        throw runtime_error("Unable to create SASL connection context.");
    }

    sasl_security_properties_t secprops;
    std::memset(&secprops, 0L, sizeof(secprops));
    secprops.maxbufsize = 2048;
    secprops.min_ssf = 0;
    secprops.max_ssf = 16777216;
    secprops.security_flags = SASL_SEC_NOPLAINTEXT | SASL_SEC_NOANONYMOUS;

    result = sasl_setprop(mContext, SASL_SEC_PROPS, &secprops);

    if (result != SASL_OK)
        throw runtime_error("Setting security properties");
    if (mDebug > 1) cout << "exiting open" << endl;
}

//======================================  Read a data buffer
void auth_server::get(socket_api &s, buffer &b) {
    b.clear();
    int len = s.gets(b.data(), b.capacity());
    if (len < 0) {
        string msg = "auth_server::get() error in ";
        msg += s.where();
        throw std::runtime_error(msg /*, s.sys_errno() */);
    }
    b.push(len);
    unsigned int raw_len = 0;
    sasl_decode64(b.data(), len, b.data(), b.capacity(), &raw_len);
    b.resize(raw_len);
    if (mDebug > 2)
        cout << "auth_server: Received " << raw_len << " byte message." << endl;
}

//======================================  Put a data buffer.
void auth_server::put(socket_api &s, const char *out, unsigned int outlen) {
    if (mDebug > 2)
        cout << "auth_server: Sending " << outlen << " byte message." << endl;
    int l64 = ((outlen / 3) + 1) * 4 + 2;
    std::vector<char> enc_str(l64);
    //lcl_array<char> enc_str(l64);
    unsigned int enc_len = 0;
    sasl_encode64(out, outlen, &enc_str[0], l64, &enc_len);
    enc_str[enc_len++] = '\n';
    if (s.write(&enc_str[0], enc_len) < 0) {
        string msg = "auth_server::put() error in ";
        msg += s.where();
        throw std::runtime_error(msg /*, s.sys_errno()*/);
    }
}

//======================================  Set the debug message verbosity
void auth_server::setDebug(int dlvl) { mDebug = dlvl; }
