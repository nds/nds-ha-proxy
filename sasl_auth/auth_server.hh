/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef AUTH_SERVER_HH
#define AUTH_SERVER_HH

#include <sasl/sasl.h>
#include <stdexcept>
#include <string>

namespace sends {

class socket_api;
class buffer;

/**  sasl_server is a global sasl system device
  *  @memo SASL global authentication device.
  *  @author John Zweizig (john.zweizig@ligo.org)
  *  @version 1.0; Last modified May 16, 2010
  */
class sasl_server {
  public:
    /**  Sasl_server constructor.
      */
    sasl_server(void);

    /**  Sasl_server destructor.
      */
    ~sasl_server(void);

    /**  Add the specified protocol to the avalable protocol list.
      *  @param p protocol name.
      */
    void addProtocol(const std::string &p);

    /**  Get the allowed list of mechanisms
     */
    const std::string &getMechanism(void) const;

    /**  Return a pointer to the fully qualified domain name (fqdn)
      *  or NULL if the name is empty.
      *  @return Constant character pointer to domain name stirng or NULL.
      */
    const char *fqdn(void) const;

    /**  Perform global initialization.
      *  @memo Global initialization.
      */
    static void initialize(const char *server, const char *plugin_path);
    static void clean_up();

    /**  Get the kerberos realm to which this server belongs.
      *  @return Constant character pointer to realm name stirng or NULL.
      */
    const char *realm(void) const;

    /**  Get a constant reference to a streing containing the
      *  server name.
      *  @return Constant reference to the server name string.
      */
    const std::string &refServer(void) const;

    /**  Get the service name.
      *  @return constant service name character string.
      */
    const char *service(void) const;

    /**  Set the server fully qualified domain name (FQDN).
      *  @param f Fully qualified domain name string.
      */
    void setFQDN(const std::string &f);

    /**  Set the kerberos realm to which this server belongs.
      *  @param r Kerberos realm name.
      */
    void setRealm(const std::string &r);

    /**  Set the server name
      *  @param p Server name string.
      */
    void setServer(const std::string &p);

  private:
    //thread::readwritelock mux;
    std::string mServer;
    std::string mFQDN;
    std::string mRealm;
    std::string mMechanism;
    bool mInitialized;
};

//==================================  inline functions
inline const char *sasl_server::fqdn(void) const {
    if (mFQDN.empty())
        return 0;
    return mFQDN.c_str();
}

inline const std::string &sasl_server::getMechanism(void) const {
    return mMechanism;
}

inline const char *sasl_server::realm(void) const { return mRealm.c_str(); }

inline const std::string &sasl_server::refServer(void) const { return mServer; }

inline const char *sasl_server::service(void) const { return mServer.c_str(); }

/**  The auth_server class performs server-side user authentication using
  *  the sasl package.
  *  @brief Server side authorization.
  *  @author John Zweizig
  *  @version 1.0; Last Modified July 8, 2008
  *  @ingroup sends_base
  */
class auth_server {
  public:
    /** Enumerated valid authentication mechanisms.
      */
    enum auth_type {
        ///  No authentication is used.
        kNone,
        ///  Use SASL authentication.
        kSasl
    };

    typedef std::string user_data;

  public:
    /**  Construct an authenticator.
      *  @param type Authentication mechanism.
      */
    auth_server(auth_type type = kSasl);

    /**  Destroy the authenticator.
      */
    ~auth_server(void);

    /**  Authenticate a client on the specified socket.
      *  @param s socket number.
      *  @param buf temporary buffer for communications use.
      */
    void authenticate(socket_api &s, buffer &buf);

    /**  Authenticate a client using sasl.
      *  @param s socket number.
      *  @param buf temporary buffer for communications use.
      */
    void auth_sasl(socket_api &s, buffer &buf);

    /**  Close the authenticator and delete its context.
      */
    void close(void);

    /**  Get a list of the allowed mechanisms that are available to
      *  this context.
      *  @param m Server name string.
      */
    std::string getMechanismList(void) const;

    /**  Start the authentication process. Create and initialize a
      *  context structure appropriate to the chosen mechanism.
      *  @exception runtime_error is thrown if open() is unable to
      *             construct the context structure or set the
      *             required properties.
      */
    void open(void);

    /**  Get a reference to the sasl server.
      */
    static sasl_server &refGlobal(void);

    /** Get a reference to the user
      */
    const user_data &refUser() const;

        /**  Test whether the authetication technique is secure.
      *  @brief Test for secure authentication.
      *  @return True if a secure authentication mechanism is used.
      */
    bool secure(void) const;

    /**  Set debug printout level.
      *  \param dlvl Debug printout level.
      */
    void setDebug(int dlvl);

  private:
    /**  Receive and uncompress/decode a string from the client.
      *  @brief Receive a string.
      *  @param s Reference to the server socket interface.
      *  @param buf Buffer to receive the decoded text string.
      */
    void get(socket_api &s, buffer &buf);

    /**  Compress/encode a string and send it to the client.
      *  @brief Send a string.
      *  @param s Reference to the server socket interface.
      *  @param out Text string to be sent.
      *  @param len Length of the text string.
      */
    void put(socket_api &s, const char *out, unsigned int len);

  private:
    static sasl_server global;

  private:
    auth_type mAuthType;
    user_data mAuthUser;
    sasl_conn_t *mContext;
    int mDebug;
};

//==================================  Inline methods.
inline sasl_server &auth_server::refGlobal(void) { return global; }

inline const auth_server::user_data &auth_server::refUser(void) const { return mAuthUser; }

inline bool auth_server::secure(void) const { return mAuthType == kSasl; }

} // namespace sends.

#endif // if !defined(AUTH_SERVER_HH)
