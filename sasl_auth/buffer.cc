/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "buffer.hh"

using namespace sends;

//====================================== Constructor
buffer::buffer(size_type length)
    : mData(0), mLength(0), mCapacity(0) {
    reserve(length);
}

//====================================== Destructor
buffer::~buffer(void) { purge(); }

//====================================== Set data length to zero
void buffer::clear(void) { mLength = 0; }

//======================================  Scour the data vector.
void buffer::power_clean(void) {
    for (size_type i = 0; i < mCapacity; ++i) {
        mData[i] = 0;
    }
    mLength = 0;
}

//======================================  Free the data vector
void buffer::purge(void) {
    if (mData)
        delete[] mData;
    mData = 0;
    mCapacity = 0;
    mLength = 0;
}

//======================================  Append data
buffer::size_type buffer::push(size_type n) {
    mLength += n;
    if (mLength > mCapacity)
        mLength = mCapacity;
    return mLength;
}

//======================================  Reserve a larger data vector.
void buffer::reserve(size_type length) {
    if (mCapacity < length) {
        purge();
        mData = new char[length];
        mCapacity = length;
    }
}

//======================================  Reset the data length.
void buffer::resize(size_type length) {
    if (length < mCapacity) {
        mLength = length;
    } else {
        mLength = mCapacity;
    }
}
