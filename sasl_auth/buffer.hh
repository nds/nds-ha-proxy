/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef SENDS_BUFFER_HH
#define SENDS_BUFFER_HH

#include <string>

namespace sends {

/**  The buffer class defines a preallocated fixed length buffer
  *  with a variable current size pointer.
  *  @brief Preallocated buffer class.
  *  @author John Zweizig
  *  @version 1.0; Last modified January 23, 2008
  *  @ingroup sends_base
  */
class buffer {
  public:
    /**  Data type used for length variables.
      */
    typedef unsigned long size_type;

  public:
    /**  Construct a buffer with the specified length. The current length
      *  pointer is set to zero.
      *  @brief Constructor.
      *  @param length Buffer capacity in bytes.
      */
    buffer(size_type length);

    /**  Destroy a buffer.
      *  @brief Destructor
      */
    ~buffer(void);

    /**  Return the allocated capacity of the buffer.
      *  @brief Buffer capacity.
      *  @return Buffer capacity (defined at allocation time).
      */
    size_type capacity(void) const;

    /**  Zero the current data length. The buffer remains allocated.
      *  @brief Clear data buffer.
      */
    void clear(void);

    /**  Return a constant pointer to the buffer data area.
      *  @brief Data pointer.
      *  @return Constant pointer to the buffer data area.
      */
    const char *data(void) const;

    /**  Return a pointer to the buffer data area.
      *  @brief Data pointer.
      *  @return Pointer to the buffer data area.
      */
    char *data(void);

    /**  Return the buffer to its free list. The buffer remains allocated.
      *  @brief Return buffer to the appropriate free list.
      */
    void free(void);

    /**  Return a referece to the ith byte in the data buffer.
      *  @brief Ith data byte.
      *  @param inx Index of character to be referenced.
      *  @return Reference to the ith data byte.
      */
    char &operator[](size_type inx);

    /**  Return a constant reference to the ith byte in the data buffer.
      *  @brief Ith data byte.
      *  @param inx Index of character to be referenced.
      *  @return Constant reference to the ith data byte.
      */
    char operator[](size_type inx) const;

    /**  Rewrite the entire buffer with zeroes and set the current data
      *  length to zero.
      *  @brief Scour data from buffer.
      */
    void power_clean(void);

    /**  Deallocate the buffer data vector and set the buffer capacity
      *  and current data lenth to zero.
      *  @brief Purge the buffer.
      */
    void purge(void);

    /**  Increase the current data length by the specified number of bytes.
      *  if the resulting length is greater than the buffer capacity, the
      *  data length is set to the buffer capacity.
      *  @brief Increment the data length pointer.
      *  @param n Number of bytes to add to the buffer data length.
      *  @return New data length.
      */
    size_type push(size_type n);

    /**  Change the current data length of the buffer. The data contents
      *  and the capacity of the buffer remain unchanged. If the
      *  requested size is greater than the capacity of the buffer,
      *  the data length is set to the buffer length.
      *  @brief Reset the data length.
      *  @param n New data length value.
      */
    void resize(size_type n);

    /**  Expand the buffer capacity to the specified number of bytes.
      *  Any data currently contain in the buffer are lost and the
      *  current data length is set to zero. No action is taken if the
      *  current capacity of the buffer is greater than or equal to the
      *  requested length.
      *  @brief Reserve specified length buffer.
      *  @param n New length of buffer.
      */
    void reserve(size_type n);

    /**  Get the current data length.
      *  @brief Data length.
      *  @return Data length in bytes.
      */
    size_type size(void) const;

    /**  Get buffer contents as a string
      *  @brief Get buffer string.
      *  @return String with buffer contents.
      */
    std::string str(void) const;

  private:
    char *mData;
    size_type mLength;
    size_type mCapacity;
};

inline buffer::size_type buffer::capacity(void) const { return mCapacity; }

inline char *buffer::data(void) { return mData; }

inline const char *buffer::data(void) const { return mData; }

inline char buffer::operator[](size_type inx) const { return mData[inx]; }

inline char &buffer::operator[](size_type inx) { return mData[inx]; }

inline buffer::size_type buffer::size(void) const { return mLength; }

inline std::string buffer::str(void) const {
    return std::string(mData, mLength);
}
}

#endif // !defined(SENDS_BUFFER_HH)
