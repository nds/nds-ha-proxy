/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "raw_socket.hh"
#include <cstdio>
#include <errno.h>
#include <fcntl.h>
#include <iomanip>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <sstream>
#include <sys/socket.h>
#include <unistd.h>

/*--------------------------------------------------------------------
 *
 *    Select includes depend on posix versions
 *--------------------------------------------------------------------*/
#if _POSIX_C_SOURCE < 200100L
#include <sys/time.h>
#include <sys/types.h>
#else
#include <sys/select.h>
#endif

using namespace sends;

//======================================  Raw socket constructor
raw_socket::raw_socket(void) : mWTimeout(32.0), mSocket(-1), mSaveOFlags(0) {
    mSocket = ::socket(PF_INET, SOCK_STREAM, 0);
    if (mSocket < 0) {
        set_error(serr_failure, "constructor");
        if (verbose())
            perror("Failed to open raw_socket");
    }
}

//======================================  Raw socket constructor
raw_socket::raw_socket(int fd) : mWTimeout(32.0), mSocket(fd) {}

//======================================  Raw socket destructor
raw_socket::~raw_socket(void) { close(); }

void raw_socket::release() {
    mSocket = -1;
}

//======================================  Accept a connection.
raw_socket *raw_socket::accept(void) {
    raw_socket *rs = 0;

    //----------------------------------  Set up the address return
    sockaddr_in sock;
    sock.sin_family = AF_INET;
    sock.sin_addr.s_addr = 0;
    sock.sin_port = 0;
    socklen_t len = 16;

    //----------------------------------  accept the connection
    int fd = ::accept(mSocket, (struct sockaddr *)&sock, &len);
    if (fd < 0) {
        mPeer.clear();
        set_error("accept");
    } else {
        rs = new raw_socket(fd);
        std::ostringstream peer;
        const unsigned char *p =
            reinterpret_cast<unsigned char *>(&sock.sin_addr.s_addr);
        peer << int(p[0]) << "." << int(p[1]) << "." << int(p[2]) << "."
             << int(p[3]) << ":" << sock.sin_port;
        rs->mPeer = peer.str();
        set_error(serr_OK);
    }
    return rs;
}

//======================================  Bind a socket to a port
raw_socket::sock_err raw_socket::bind(const std::string &port) {
    sockaddr_in sock;
    sock_err ad_err = parse_addr(port, sock.sin_addr.s_addr, sock.sin_port);
    if (ad_err) {
        return set_error(ad_err, "parse_addr");
    } else if (verbose() > 1) {
        std::cerr << "raw_socket: bind address " << std::hex
                  << ntohl(sock.sin_addr.s_addr) << std::dec << ":"
                  << ntohs(sock.sin_port) << std::endl;
    }
    sock.sin_family = AF_INET;
    socklen_t len = 16;
    int rc = ::bind(mSocket, (struct sockaddr *)&sock, len);
    if (rc < 0)
        return set_error("bind");
    set_error(serr_OK);
    return serr_OK;
}

//======================================  Get blocking mode.
bool raw_socket::blocking(void) const { return (oflags() & O_NONBLOCK) == 0; }

//======================================  Close the socket
raw_socket::sock_err raw_socket::close(void) {
    set_error(serr_OK);
    if (mSocket >= 0) {
        ::shutdown(mSocket, SHUT_RDWR);
        ::close(mSocket);
        mSocket = -1;
    }
    return error();
}

//======================================  Connect to a foreign socket
raw_socket::sock_err raw_socket::connect(const std::string &peer) {
    mPeer = peer;
    sock_err rc = serr_OK;
    return rc;
}

//======================================  Listen for connection requests
raw_socket::sock_err raw_socket::listen(const std::string &port, int qlen) {
    sock_err rc = bind(port);
    if (rc == serr_OK)
        return listen(qlen);
    return rc;
}

//======================================  Listen for connection requests
raw_socket::sock_err raw_socket::listen(int qlen) {
    sock_err rc = serr_OK;
    if (::listen(mSocket, qlen)) {
        rc = set_error("listen");
    }
    return rc;
}


//======================================  Get the open flag-word contents
raw_socket::oflag_type raw_socket::oflags(void) const {
    return fcntl(mSocket, F_GETFL, 0);
}

//======================================  wait for an event.
int raw_socket::wait_event(wait_mode wm, double wt) {
    struct timeval tspec, *tptr;
    if (wt < 0) {
        tptr = 0;
    } else {
        long ws = long(wt);
        tspec.tv_sec = ws;
        tspec.tv_usec = int(100000.0 * (wt - double(ws)));
        tptr = &tspec;
    }

    fd_set sockfds;
    FD_ZERO(&sockfds);
    FD_SET(mSocket, &sockfds);

    set_error(serr_OK);
    int rc = -1;
    while (rc < 0) {
        switch (wm) {
        case wm_read:
            rc = ::select(mSocket + 1, &sockfds, 0, 0, tptr);
            break;
        case wm_write:
            rc = ::select(mSocket + 1, 0, &sockfds, 0, tptr);
            break;
        case wm_control:
            rc = ::select(mSocket + 1, 0, 0, &sockfds, tptr);
            break;
        }
        if (rc < 0 && errno != EINTR)
            break;
    }
    if (rc < 0) {
        set_error("wait_event");
    }
    return rc;
}

//======================================  Read available data
long raw_socket::read_available(char *buf, long len) {
    set_error(serr_OK);
    oflag_type flags = oflags();
    if (flags < 0)
        return set_error("read_available");
    set_oflags(flags | O_NONBLOCK);
    long nB = ::read(mSocket, buf, len);
    if (nB < 0) {
        set_error("read_available");
    }
    set_oflags(flags);
    return nB;
}

//======================================  Restore oflags to the previous state
raw_socket::sock_err raw_socket::restore_oflags(void) {
    return set_oflags(mSaveOFlags);
}

//======================================  Set/clear reuse-address option
raw_socket::sock_err raw_socket::reuse_addr(bool reuse) {
    if (reuse)
        return set_option(SO_REUSEADDR, 1);
    else
        return set_option(SO_REUSEADDR, 1);
}

//======================================  Set a specified socket options.
raw_socket::sock_err raw_socket::set_blocking(bool yorn) {
    mSaveOFlags = oflags();
    oflag_type newflag = mSaveOFlags;
    if (yorn)
        newflag &= ~O_NONBLOCK;
    else
        newflag |= O_NONBLOCK;
    if (newflag != mSaveOFlags)
        set_oflags(newflag);
    return serr_OK;
}

//======================================  Set a specified socket options.
raw_socket::sock_err raw_socket::set_oflags(oflag_type flagwd) {
    if (::fcntl(mSocket, F_SETFL, flagwd) < 0)
        return set_error("set_oflags");
    return serr_OK;
}

//======================================  Set a specified socket options.
raw_socket::sock_err raw_socket::set_option(int id, int val) {
    int rc = setsockopt(mSocket, SOL_SOCKET, id, &val, sizeof(int));
    if (rc < 0)
        return set_error("setsockopt");
    set_error(serr_OK);
    return serr_OK;
}

//======================================  Test if a socket is still connected
bool raw_socket::test_connect(void) {
    if (!is_open())
        return false;
    //  maybe a wait(0, control) would work best??
    char buf[1];
    // first set non-blocking (ioctrl)
    set_blocking(false);
    int err = recv(mSocket, buf, 1, MSG_PEEK);
    bool rc = (err >= 0 || errno == EWOULDBLOCK);
    //  restore blocking flag (ioctrl)
    restore_oflags();
    return rc;
}

//====================================  Wait routine...
int raw_socket::wait_data(double wt) { return wait_event(wm_read, wt); }

//======================================  Raw socket constructor
long raw_socket::write(const char *buf, long len) {
    long nB = 0;
    while (nB < len) {
        int wok = wait_event(wm_write, mWTimeout);
        if (wok <= 0) {
            nB = -1;
            if (wok)
                set_error("write");
            else
                set_error(serr_timeout, "write");
            break;
        }
        long wrc = ::write(mSocket, buf + nB, len - nB);
        if (wrc > 0) {
            nB += wrc;
        } else if (set_error("write") != serr_retry) {
            nB = -1;
            break;
        }
    }
    if (nB > 0)
        set_error(serr_OK);
    return nB;
}
