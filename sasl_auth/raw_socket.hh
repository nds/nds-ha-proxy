/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef SENDS_RAW_SOCKET_HH
#define SENDS_RAW_SOCKET_HH

#include "socket_api.hh"

namespace sends {

/**  The default socket api stuff.
  *  \brief Socket API.
  *  \author J. Zweizig
  *  \version $Id$
  */
class raw_socket : public socket_api {
  public:
    /**  Raw socket constructor
      *  \brief Constructor
      */
    raw_socket(void);

    /**  Construct a raw_socket from an existing unix socket.
      *  \brief construct a socket
      *  \param fd File descriptor for existing socket.
      */
    raw_socket(int fd);

    /**  Disconnect and close the socket. Destroy the socket object.
      *  \brief Destructor.
      */
    ~raw_socket(void);

    void release();

    /**  Accept a requested connection and create a client socket.
      *  \brief Accept a connection request.
      *  \return Client socket pointer if accept succedes, NULL otherwise.
      */
    raw_socket *accept(void);

    /**  Bind the socket to the specified IP and port number.
      *  \brief Bind the socket to a port.
      *  \param port Port name string in the form "<ip-addr>:<port>"
      *  \return Socket error code
      */
    sock_err bind(const std::string &port);

    /**  Test the socket (non-)blocking mode.
      *  \brief Test blocking mode.
      *  \return True if blocking.
      */
    virtual bool blocking(void) const;

    /**  Disconnect and close the socket.
      *  \brief Close the socket.
      *  \return Socket error code
      */
    sock_err close(void);

    /**  Connect the socket to the specified peer IP and port number.
      *  \brief Connect the socket to a peer.
      *  \param peer Port name string in the form "<ip-addr>:<port>"
      *  \return Socket error code
      */
    sock_err connect(const std::string &peer);

    /**  Test for open socket.
      *  \brief Test socket open.
      *  \return True if socket is open
      */
    bool is_open(void) const;

    /**  Listen to the specified port address, Queue the specified
      *  number of connection requests pending acceptance.
      *  \brief Listen for connection requests.
      *  \param port Local port name to listen to.
      *  \param lq   Length of connection request queue
      *  \return Socket error code.
      */
    sock_err listen(const std::string &port, int lq);
    sock_err listen(int lq);

    /**  Read up to the specified number of bytes from the socket into
      *  the buffer.
      *  \brief Read available data.
      *  \param buf  Pointer to buffer to receive data.
      *  \param len Maximum length of data to be read (in bytes).
      *  \return Number of data bytes read.
      */
    virtual long read_available(char *buf, long len);

    /**  Set the socket REUSEADDR state to the specified boolean value.
      *  \brief Set/clear the reuse-address state.
      *  \param reuse Set re-use address if true.
      */
    sock_err reuse_addr(bool reuse);

    /**  Set the socket to blocking or non-blocking mode.
      *  \brief Set blocking mode.
      *  \param yorn New mode for blocking
      *  \return Zero on success.
      */
    virtual sock_err set_blocking(bool yorn);

    /**  Set a socket option.
      *  \brief Set an option.
      *  \param id  Option identifier
      *  \param val Option value;
      */
    sock_err set_option(int id, int val);

    /**  Get a socket id number. In general this will be the system fd
      *  for the socket.
      *  \brief Get the socket id.
      *  \return Socket id for use in select.
      */
    virtual int socket_id(void) const;

    /**  Test whether the socket is (still) connected.
      *  \brief test connection
      *  \return True if connected.
      */
    bool test_connect(void);

    /**  Wait at most the specified number of seconds for data
      *  to arrive for reading.
      *  \brief Wait for data.
      *  \param nsec Maximum number of seconds to wait for data
      *  \return Integer with the following values:
      *      - -1 Systerm error occurred (e.g. EINTR)
      *      -  0 Request timed out.
      *      -  1 data available on port.
      */
    int wait_data(double nsec);

    /**  Write the specified number of bytes into the buffer.
      *  \brief Write data.
      *  \param buf  Pointer to data to be written
      *  \param len Length of data to be written (in bytes).
      *  \return Number of data bytes written or -1 on error.
      */
    virtual long write(const char *buf, long len);

  protected:
    /// Open flag word data type.
    typedef long oflag_type;

    /// The wait_mode enumerator specifies which event will be waited for
    enum wait_mode { wm_read, wm_write, wm_control };

  protected:
    /**  Get the socket open flags.
      *  \brief Open flags.
      *  \return Current state of open flags.
      */
    oflag_type oflags(void) const;

    /**  Wait for a read, write or control socket event.
      *  \brief Wait for a specified state
      *  \param wm Event type to wait for.
      *  \param wt Wait time.
      *  \return Result code with on of the following values:
      *      - -1 Systerm error occurred (e.g. EINTR)
      *      -  0 Request timed out.
      *      -  1 data available on port.
      */
    int wait_event(wait_mode wm, wtime_type wt);

    /**  Restore the socket open flags to the state before the last
      *  blocking change.
      *  \brief set socket open-flags.
      *  \return Error code.
      */
    sock_err restore_oflags(void);

    /**  Get the socket open flags.
      *  \brief set socket open-flags.
      *  \param flagwd new open-flags word.
      *  \return Error code.
      */
    sock_err set_oflags(oflag_type flagwd);

  protected:
    wtime_type mWTimeout;   ///< Write timeout
    int mSocket;            ///<  Opened socket file number or -1.
    oflag_type mSaveOFlags; ///< save oflags
};

//==================================  Inline methods.
inline int raw_socket::socket_id(void) const { return mSocket; }

inline bool raw_socket::is_open(void) const { return (mSocket >= 0); }

} // namespace sends

#endif // !defined(SENDS_RAW_SOCKET_HH)
