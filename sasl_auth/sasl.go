package sasl_auth

// #cgo LDFLAGS: -lsasl2
// #include "nds_sasl.h"
// #include <stdlib.h>
import "C"
import (
	"errors"
	"git.ligo.org/nds/nds-ha-proxy/config"
	"log"
	"os"
	"unsafe"
)

func InitSasl(cfg *config.Config) bool {
	var authPtr *config.ConfigAuth = nil
	for i, _ := range cfg.Listeners {
		if cfg.Listeners[i].Auth != nil && len(cfg.Listeners[i].Auth.Mechanisms) > 0 {
			if authPtr != nil {
				log.Fatal("We only allow one SASL protected endpoint at most.\n")
			}
			authPtr = cfg.Listeners[i].Auth
		}
	}
	if authPtr == nil {
		return false
	}
	service := C.CString(authPtr.ServiceName)
	defer C.free(unsafe.Pointer(service))
	plugin_path := C.CString(cfg.SaslPlugins)
	defer C.free(unsafe.Pointer(plugin_path))

	if int(C.nds_sasl_initialize(service, plugin_path)) != 1 {
		log.Fatal("Unable to initialize SASL")
	}
	if authPtr.ServiceName != "" {
		C.nds_sasl_auth_set_server(service)
	}
	if authPtr.FQDN != "" {
		fqdn := C.CString(authPtr.FQDN)
		defer C.free(unsafe.Pointer(fqdn))
		C.nds_sasl_auth_set_fqdn(fqdn)
	}
	if authPtr.Realm != "" {
		realm := C.CString(authPtr.Realm)
		defer C.free(unsafe.Pointer(realm))
		C.nds_sasl_auth_set_realm(realm)
	}
	if authPtr.Krb5Keytab != "" {
		keytab := C.CString(authPtr.Krb5Keytab)
		defer C.free(unsafe.Pointer(keytab))
		C.nds_sasl_auth_set_keytab(keytab)
	}
	if authPtr.Krb5Conf != "" {
		conf := C.CString(authPtr.Krb5Conf)
		defer C.free(unsafe.Pointer(conf))
		C.nds_sasl_auth_set_krb5conf(conf)
	}
	for _, protocol := range authPtr.Mechanisms {
		mech := C.CString(protocol)
		C.nds_sasl_auth_add_protocol(mech)
		C.free(unsafe.Pointer(mech))
	}
	return true
}

func CloseSasl() {
	C.nds_sasl_clean_up()
}

type SaslSession struct {
	session C.nds_sasl_auth
}

func SaslAuth(f *os.File) (*SaslSession, error) {
	context := C.nds_sasl_authenticate(C.int(f.Fd()))
	if context == nil {
		return nil, errors.New("Unable to authenticate user")
	}
	return &SaslSession{context}, nil
}

func (ss *SaslSession) Close() {
	C.nds_sasl_auth_clean_up(ss.session)
	ss.session = nil
}

func (ss *SaslSession) GetUser() string {
	return C.GoString(C.nds_sasl_auth_userref(ss.session))
}
