/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "socket_api.hh"
#include "timer.hh"

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

using namespace std;

namespace sends {

//====================================  Default constructor
socket_api::socket_api(void) : mError(serr_OK), mVerbose(0), mTimeout(-1) {}

//====================================  Destructor
socket_api::~socket_api(void) {}

//====================================  Read the specified number of bytes.
long socket_api::gets(char *buf, long maxlen, char term) {
    Timer egg_timer;
    long nB = 0;
    wtime_type tLeft = mTimeout;
    while (nB < maxlen) {
        if (wait_data(tLeft) < 0) {
            nB = -1;
            break;
        }
        int nread = read_available(buf + nB, 1);
        if (nread < 0) {
            if (error() != serr_retry) {
                if (nB)
                    cerr << "socket_api::gets bailed after '" << string(buf, nB)
                         << "'" << endl;
                nB = -1;
                break;
            }
        } else if (!nread) {
            set_error(serr_enddata, "gets");
            break;
        } else if (buf[nB++] == term) {
            buf[--nB] = 0;
            break;
        }
        if (mTimeout >= 0) {
            wtime_type elap = egg_timer.elapsed();
            if (elap >= mTimeout) {
                set_error(serr_timeout, "gets");
                break;
            }
            tLeft = mTimeout - elap;
        }
    }
    if (verbose() > 3) {
        cout << "gets len=" << nB << " string: " << buf << endl;
    }
    return nB;
}

//====================================  Handshake with partner.
socket_api::sock_err socket_api::handshake(void) {
    sock_err rc = serr_OK;
    return rc;
}

//====================================  Parse an IP address
socket_api::sock_err socket_api::parse_addr(const string &addr,
                                            unsigned int &node,
                                            unsigned short &port) {

    //--------------------------------  Pick off the port number
    port = 0;
    string::size_type inx = addr.find(":");
    if (inx != string::npos) {
        port = htons(strtoul(addr.c_str() + inx + 1, 0, 0));
    }

    //--------------------------------  Get the address.
    if (!inx) {
        node = INADDR_ANY;
    } else {
        string host = addr.substr(0, inx);
#if defined(__linux__) || defined(__APPLE__)
        struct hostent *hentp = gethostbyname(host.c_str());
        if (!hentp) {
            perror("parse_addr: error in gethostbyname");
            return serr_failure;
        }
        memcpy(&node, *hentp->h_addr_list, sizeof(node));
#else
#define HOST_BUF_LENGTH 2048
        struct hostent hent;
        char buf[HOST_BUF_LENGTH];
        int gherr;
#ifdef __sun__
        struct hostent *rc; // solaris
#else
        int rc;
#endif
        rc = gethostbyname_r(host.c_str(), &hent, buf, HOST_BUF_LENGTH, &gherr);
        if (!rc) {
            perror("parse_addr: error in gethostbyname_r");
            return serr_failure;
        }
        memcpy(&node, *hent.h_addr_list, sizeof(node));
#endif
    }
    return serr_OK;
}

//====================================  Write a hex string
int socket_api::put_hex(long in, int nDigit) {
    char list[16];
    for (int i = nDigit; i > 0; --i) {
        char digit = (in & 15);
        if (digit < 10)
            digit += '0';
        else
            digit += 'a' - 10;
        list[i - 1] = digit;
        in >>= 4;
    }
    return write(list, nDigit);
}

//====================================  Write an integer.
int socket_api::put_int(int32_t in) {
    int32_t out = htonl(in);
    return write(reinterpret_cast<char *>(&out), sizeof(out));
}

//====================================  Read the specified number of bytes.
long socket_api::read(char *buf, long maxlen) {
    Timer egg_timer;
    long nB = 0;
    wtime_type tLeft = mTimeout;
    while (nB < maxlen) {
        wait_data(tLeft);
        int len = read_available(buf + nB, maxlen - nB);
        if (len > 0) {
            nB += len;
        } else if (error() != serr_retry) {
            nB = -1;
            break;
        }
        if (mTimeout >= 0) {
            wtime_type elap = egg_timer.elapsed();
            if (elap >= mTimeout)
                break;
            tLeft = mTimeout - elap;
        }
    }
    return nB;
}

//====================================  Set the socket error code
socket_api::sock_err socket_api::set_error(sock_err err,
                                           const std::string &where) {
    mError = err;
    mSysError = errno;
    mWhere = where;
    if (mVerbose) {
        string msg = "Error occurred in ";
        msg += mWhere;
        perror(msg.c_str());
    }
    return mError;
}

//====================================  Guess an error code from errno
socket_api::sock_err socket_api::set_error(const std::string &where) {
    sock_err err = serr_failure;
    switch (errno) {
    case 0:
        err = serr_OK;
        break;
    case EAGAIN:
    case EINTR:
        err = serr_retry;
        break;
    case ENOTSOCK:
    case EBADF:
    case EOPNOTSUPP:
    case EFAULT:
        err = serr_internal;
        break;
    case EPIPE:
    default:
        err = serr_failure;
    }
    return set_error(err, where);
}

//====================================  Set the timout time
void socket_api::set_timeout(wtime_type timeout) { mTimeout = timeout; }

//====================================  Set the verbose printout flag
void socket_api::set_verbose(int verb) { mVerbose = verb; }

//====================================  Dump wait routine... Assume data.
int socket_api::wait_data(double nsec) { return 1; }

} // namespace sends
