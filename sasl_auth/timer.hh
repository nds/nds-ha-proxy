/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef SENDS_EGG_TIMER_HH
#define SENDS_EGG_TIMER_HH

#include <time.h>

/**  The Timer class measures the elapsed real time from the construction
  *  Timer to the time(s) it is polled.
  *  \brief Measure elapsed real time.
  *  \author John Zweizig
  *  \version 1.0; last modified 2008-07-28
  */
class Timer {
  public:
    /**  Data type for elapsed time in seconds.
      */
    typedef double wtime_type;

  public:
    /**  Construct a Timer and store the starting time.
      *  \brief Timer constructor.
      */
    Timer(void);

    /**  Destroy the Timer.
      *  \brief Timer destructor.
      */
    ~Timer(void);

    /**  Return the elapsed real time between the construction of the
      *  timer to the present.
      *  \brief Get the elapsed real-time.
      *  \return Elapsed real time in seconds.
      */
    wtime_type elapsed(void) const;

  private:
    /** Timer start time.
      */
    struct timespec mStartTime;
};

#endif // !defined(SENDS_EGG_TIMER_HH)
