package nds_ha_proxy

import (
	"errors"
	"git.ligo.org/nds/nds-ha-proxy/config"
	"log"
	"net"
	"sync"
	"time"
)

type Server struct {
	transport string
	address   string
	is_up     bool
}

type PhysicalServer struct {
	server Server
	weight int
}

type ServerList struct {
	servers            []int
	phys_servers       []PhysicalServer
	lock               sync.Mutex
	cur                int
	avail_phys_servers int
}

func CreateServerList(cfg *config.Config) *ServerList {
	var num_phys_servers = len(cfg.Servers)
	sl := &ServerList{servers: make([]int, 0, 5), phys_servers: make([]PhysicalServer, num_phys_servers, num_phys_servers)}
	var max_weight = 1
	for cur_server, server := range cfg.Servers {
		if server.Weight > max_weight {
			max_weight = server.Weight
		}
		weight := server.Weight
		if weight < 1 {
			weight = 1
		}
		sl.phys_servers[cur_server] = PhysicalServer{server: Server{transport: server.Transport, address: server.Address, is_up: true}, weight: weight}
	}
	sl.avail_phys_servers = len(sl.phys_servers)
	for cur_weight := 1; cur_weight <= max_weight; cur_weight++ {
		log.Println("Adding servers with weight ", cur_weight)
		for cur_server, server := range sl.phys_servers {
			if server.weight < cur_weight {
				continue
			}
			sl.servers = append(sl.servers, cur_server)
		}
	}
	for i, server := range sl.phys_servers {
		log.Printf("%d: %s:%d\n", i, server.server.address, server.weight)
	}
	log.Println("Server choice distribution")
	for _, index := range sl.servers {
		log.Printf("%d ", index)
	}
	return sl
}

func (sl *ServerList) NextServer() (Server, error) {
	sl.lock.Lock()
	defer sl.lock.Unlock()

	max_servers := len(sl.servers)

	for tries := 0; tries < max_servers; tries++ {
		if sl.cur >= max_servers-1 {
			sl.cur = 0
		} else {
			sl.cur++
		}
		if sl.phys_servers[sl.servers[sl.cur]].server.is_up {
			return sl.phys_servers[sl.servers[sl.cur]].server, nil
		}
	}
	return Server{}, errors.New("no proxied servers are available")
}

func (sl *ServerList) setServerStatus(s Server, status bool) {
	delta := 1
	if !status {
		delta = -1
	}

	sl.lock.Lock()
	defer sl.lock.Unlock()

	for i := 0; i < len(sl.servers); i++ {
		if sl.phys_servers[i].server.address == s.address && sl.phys_servers[i].server.transport == s.transport {
			sl.phys_servers[i].server.is_up = status
			sl.avail_phys_servers += delta
			return
		}
	}
}

func (sl *ServerList) MarkServerUp(s Server) {
	sl.setServerStatus(s, true)
}

func (sl *ServerList) MarkServerDown(s Server) {
	sl.setServerStatus(s, false)
}

func (sl *ServerList) ServerCheckLoop(metrics *MetricsDatabase) {
	tries := 0
	cur := 0
	nextDown := func() (string, string, bool) {
		sl.lock.Lock()
		defer sl.lock.Unlock()

		max := len(sl.phys_servers)
		if sl.avail_phys_servers == max {
			return "", "", false
		}
		for ; tries < max; tries++ {
			cur++
			if cur >= max {
				cur = 0
			}
			if !sl.phys_servers[cur].server.is_up {
				return sl.phys_servers[cur].server.transport, sl.phys_servers[cur].server.address, true
			}
		}
		return "", "", false
	}

	serverMetrics := [2]Metric{
		{Name: "proxy.total_listeners", Value: NewGage(int64(len(sl.phys_servers)))},
		{Name: "proxy.available_listeners", Value: NewGage(0)},
	}

	go func() {
		countAvail := func() int {
			sl.lock.Lock()
			defer sl.lock.Unlock()
			return sl.avail_phys_servers
		}

		for {
			serverMetrics[1].Value.Set(int64(countAvail()))
			metrics.Update(serverMetrics[:])
			time.Sleep(30 * time.Second)
		}
	}()

	for {
		time.Sleep(30 * time.Second)

		transport, address, isDown := nextDown()
		for isDown {
			c, err := net.DialTimeout(transport, address, 15*time.Second)
			if err == nil {
				_ = c.Close()
				log.Println("Proxied server", address, "is alive")
				sl.MarkServerUp(Server{transport: transport, address: address})
			}
			transport, address, isDown = nextDown()
		}
		tries = 0
	}
}
