package nds_ha_proxy

// inspired by stdlib code
//
// Many of the net/os/... packages return special errors that
// implement a Temporary() method.  This is provided to help
// identify these errors
type temporary interface {
	Temporary() bool
}

func isErrorTemporary(err error) bool {
	if tempErr, ok := err.(temporary); ok {
		return tempErr.Temporary()
	}
	return false
}
